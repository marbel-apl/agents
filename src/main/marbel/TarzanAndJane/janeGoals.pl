wantMeeting(date(15, 02, 2010), time(18,00), duration(4,00), [jane]).

not_yet_invited(Person, Date, Time, Duration, Attendees) :-
	wantMeeting(Date, Time, Duration, Attendees), member(Person, Attendees), Person \= jane, 
	not(sentInvitation(Person, meeting(Date, Time, Duration, Attendees))).

meeting_invitation_accepted_by_all(Date, Time, Duration, Attendees) :-
	wantMeeting(Date, Time, Duration, Attendees), del(Attendees, jane, Others), 
	forall(member(X, Others), receivedAccept(X, meeting(Date, Time, Duration, Attendees))).
