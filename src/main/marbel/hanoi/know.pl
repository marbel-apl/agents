% disc(Name, size)
:-dynamic disc/2, on/3.
% Hanoi game has 3 pins (pin 0, pin 1, pin 2).
pins(2).

% Pin is empty
%empty(Pin) :- not(on(_, Pin, _)).
% Disc on pin is top disc (nothing on top of it)
topDisc(Disc,Pin) :- on(Disc, Pin, _), disc(Disc, _), not(on(_, Pin, Disc)).
