:-dynamic atFloor/1, dir/1, eButtonOn/1, fButtonOn/2, doorState/1, goTo/2.

onRoute(Level, Dir) :- atFloor(Here), dir(Dir), Diff is Level-Here, (Diff<0 -> Dir=down; Dir=up).

closerGoal(Level) :- atFloor(Here), Dist is abs(Level-Here), goTo(OtherLevel, _), OtherDist is abs(OtherLevel-Here), OtherDist<Dist.