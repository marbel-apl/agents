:-dynamic me/1, 
	have/1, % indicates that we have a product ready for use.
	have/2, % indicates a machine has a product ready for use.
	askingWhatYouCanProduce/1, % indicates that a machine is asking us what we can make.
	canProduce/1, % indicates products that we can make ourselves.
	canProduce/2, % indicates that another machine can make a certain product.
	toDeliver/2, % indicates that we want to deliver a product to a certain machine. 
	want/1, % indicates that we want a product.
	want/2. % indicates that another machine wants a product.

% Common knowledge of ingredients that are needed for making a product.
requiredFor(coffee, water).
requiredFor(coffee, grounds).
requiredFor(espresso, coffee).
requiredFor(grounds, beans).

% A machine can make a product if it has the product in stock or can make all ingredients.
canMake(Product) :- have(Product).
canMake(Product) :- canProduce(Product), forall(requiredFor(Product, Ingredient), canMake(Ingredient)).
