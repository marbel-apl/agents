:-dynamic on/2, holding/1, pickup/1, wantTower/1.

% Assume there is enough room to put all blocks on the table.
% This is actually only true for up to 13 blocks in the Tower Environment.
clear(table).
clear(X) :- block(X), not(on(_,X)), not(holding(X)).

above(X, Y) :- on(X, Y).
above(X, Y) :- on(X, Z), above(Z, Y).

tower([X]) :- on(X, table).
tower([X, Y| T]) :- on(X, Y), tower([Y| T]).

% anything that occurs as an 'on' is a block except the table.
block(X) :- on(X, _),  X \= table.
block(X) :- on(_, X),  X \= table.

% define a subtower
subTower(_,[]).
subTower(T,T).
subTower([_|T],SubT) :- subTower(T,SubT).

constructiveMove(X,Y) :- wantTower(T), subTower(T,[X,Y|Build]), tower([Y|Build]).
misplaced(X) :- tower([X|Y]), findall(X,(wantTower(T),subTower(T,[X|Y])),[]).

canPickup(X) :- clear(X), not(holding(_)).
canPutdown(X,Y) :- holding(X), clear(Y).