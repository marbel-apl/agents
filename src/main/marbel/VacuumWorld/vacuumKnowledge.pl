:-dynamic square/2.

possiblemove(left) 	:- clear(left).
possiblemove(right) 	:- clear(right).
possiblemove(back) 	:- clear(back).
possiblemove(forward) 	:- clear(forward).

% contents of squares that we can't go to.
obstruction(vac). % a bot is there
obstruction(obstacle).

clear(Dir) :- square(Dir,Obj), not(obstruction(Obj)).

% we can't see backwards so we assume it's free (may cause occasional bumps)
square(back,empty).